# ws-react-chat

A barebones, IRC-like chat app with backend and frontend code colocated
in the same repo.

The backend uses [Node][node], [Express][express], and
the WebSockets library [ws][ws]. The frontend uses [React][react], and
in particular the hooks feature, [create-react-app][create-react-app],
and [Bootstrap][bootstrap]. The app is hosted on [Heroku][heroku]. The
Heroku app provisioning was based on [these][heroku-node-and-react]
[two][heroku-node-and-ws] examples from the Heroku docs.

### [Demo][demo]

### Screenshot
![screenshot][screenshot]

[node]: https://nodejs.org/
[express]: https://github.com/expressjs/express
[ws]: https://github.com/websockets/ws
[react]: https://reactjs.org/
[create-react-app]: https://github.com/facebook/create-react-app
[bootstrap]: https://getbootstrap.com/
[heroku]: https://www.heroku.com/
[heroku-node-and-react]: https://github.com/mars/heroku-cra-node
[heroku-node-and-ws]: https://github.com/heroku-examples/node-ws-test
[demo]: https://wsrc-ian-s-mcb.herokuapp.com/
[screenshot]: https://i.imgur.com/lUjMXcb.gif
