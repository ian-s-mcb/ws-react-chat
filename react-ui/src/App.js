import React, { useEffect, useState } from 'react';

import MemberList from './components/MemberList'
import MessageList from './components/MessageList'
import ws from './utils'

function App () {
  const [messages, setMessages] = useState([])
  const [members, setMembers] = useState([])
  useEffect(() => {
    ws.onopen = e => {
      console.log('onopen')
    }
    ws.onclose = e => {
      console.log('onclose')
      setMembers([])
    }
    ws.onmessage = e => {
      console.log('onmessage')
      const message = JSON.parse(e.data)
      const { id, type, nickname, members, messageBody } = message
      setMessages([...messages, {
        type,
        nickname,
        messageBody
      }])
      setMembers(members)
      console.log(`ID: [${id}] = ${messageBody}`)
    }
    ws.onerror = e => {
      console.log('onerror')
      throw new Error('WebSocket error', e)
    }
    function sendMessage () {
      const elem = document.getElementById('input-message')
      if (ws.readyState === WebSocket.OPEN) {
        ws.send(elem.value)
      }
      elem.value = ''
      elem.focus()
    }
    document.getElementById('form-chat').onsubmit = e => {
      e.preventDefault()
      sendMessage()
    }
  })

  return (
    <div className='container-md mt-2'>
      <h1>Chat</h1>
      <div className="row">
        <div className="col-md-9">
          <h3>Messages</h3>
          <MessageList messages={messages} />
          <form id="form-chat" className="form-inline">
            <input
              id="input-message"
              className="form-control"
              type="text"
              placeholder="Type message"
            />
            <button className="btn btn-primary" type="submit">Send</button>
          </form>
        </div>
        <div className="col-md-3">
          <h3>Members</h3>
          <MemberList members={members} />
        </div>
      </div>
    </div>
  )
}

export default App
