import React from 'react'

import Message from './Message'

function MessageList ({ messages }) {
  const listItems = messages.map((message, i) => (
    <li key={i} className="list-group-item">
      <Message message={message} />
    </li>
  ))
  return (
    <ul className="list-group">
      {listItems}
    </ul>
  )
}

export default MessageList
