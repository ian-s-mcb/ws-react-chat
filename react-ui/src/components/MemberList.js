import React from 'react'

function MemberList ({ members }) {
  const listItems = members.map((member, i) => (
    <li key={i} className="list-group-item">
      {member}
    </li>
  ))
  return (
    <ul className="list-group">
      {listItems}
    </ul>
  )
}

export default MemberList
