let host
if (process.env.NODE_ENV === 'development') {
  host = 'ws://localhost:5000' // TODO replace hardcoded port
} else {
  host = window.location.origin.replace(/^http/, 'ws')
}
const ws = new WebSocket(host)

export default ws
