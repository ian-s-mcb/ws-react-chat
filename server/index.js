const express = require('express')
const WebSocket = require('ws')
const uuid = require('node-uuid')
const path = require('path')

const PORT = process.env.PORT || 5000
const app = express()

// Priority serve any static files.
app.use(express.static(path.resolve(__dirname, '../react-ui/build')))

// Answer API requests.
app.get('/api', function (req, res) {
  res.set('Content-Type', 'application/json')
  res.send('{"message":"Hello from the custom server!"}')
})

// All remaining requests return the React app, so it can handle routing.
app.get('*', function (request, response) {
  response.sendFile(path.resolve(__dirname, '../react-ui/build', 'index.html'))
})

const server = app.listen(PORT, function () {
  console.error(`Listening on port ${PORT}`)
})

const wss = new WebSocket.Server({ server })

const clients = []
let clientIndex = 1

wss.on('connection', ws => {
  const client = {
    id: uuid.v4(),
    nickname: `AnonymousUser${clientIndex}`,
    ws
  }
  clients.push(client)
  clientIndex += 1
  console.log(`client [${client.nickname}] connected`)
  wsSend('notification', `${client.nickname} has connected`)
  ws.on('message', messageBody => {
    if (messageBody.indexOf('/nick') === 0) {
      const nicknameArray = messageBody.split(' ')
      if (nicknameArray.length >= 2) {
        const oldNickname = client.nickname
        client.nickname = nicknameArray[1]
        wsSend('nickUpdate', `Client ${oldNickname} changed to ${client.nickname}`)
      }
    } else {
      wsSend('message', messageBody)
    }
  })
  ws.on('close', () => closeSocket())
  process.on('SIGINT', () => {
    console.log('Shutting down')
    closeSocket('Server has disconnected')
    process.exit()
  })
  function wsSend (type, messageBody) {
    const members = clients.map(c => c.nickname)
    const message = JSON.stringify({
      type,
      id: client.id,
      nickname: client.nickname,
      members,
      messageBody
    })
    for (let i = 0; i < clients.length; i++) {
      const clientSocket = clients[i].ws
      if (clientSocket.readyState === WebSocket.OPEN) {
        clientSocket.send(message)
      }
    }
  }
  function closeSocket (customMessageBody) {
    const i = clients.findIndex(c => c.id === client.id)
    if (i != -1) {
      const type = 'notification'
      const messageBody = customMessageBody || `${client.nickname} has disconnected`
      const members = clients
        .filter(c => c.id !== client.id)
        .map(c => c.nickname)
      const message = JSON.stringify({
        type,
        id: client.id,
        nickname: client.nickname,
        members,
        messageBody
      })
      for (let j = 0; j < clients.length; j++) {
        const clientSocket = clients[j].ws
        if (clientSocket.readyState === WebSocket.OPEN) {
          clientSocket.send(message)
        }
      }
      clients.splice(i, 1)
    }
  }
})
